1. Create the following directory structure. (Create empty files where necessary)

```
hello
├── five
│   └── six
│       ├── c.txt
│       └── seven
│           └── error.log
└── one
    ├── a.txt
    ├── b.txt
    └── two
        ├── d.txt
        └── three
            ├── e.txt
            └── four
                └── access.log
```

/*
mkdir hello
cd hello
mkdir -p five/six/seven
mkdir -p one/two/three/four
touch five/six/seven/error.log
touch five/six/c.txt
touch one/two/three/four/access.log
touch one/two/three/e.txt
touch one/two/d.txt
touch one/a.txt
touch one/b.txt
*/
2. Delete all the files with the .log extension
rm five/six/seven/error.log
rm one/two/three/four/access.log

3.Add the following content to a.txt
cd one
echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others." >> a.txt

4.Delete the directory named `five`.


rmdir five/six/seven
rm five/six/c.txt
rmdir five/six
rmdir five




------------------------------------------------------------------------------


1. Download the contents of "Harry Potter and the Goblet of fire" using the command line from https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt
```
 curl -O https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt
```

2. Print the first three lines in the book
```
   head -3 harry.txt
```
3.Print the last 10 lines in the book
```
  tail -10 harry.txt
```
4.How many times do the following words occur in the book?
```
  Harry
  grep -o -i Harry "Harry%20Potter%20and%20the%20Goblet%20of%20Firet.txt" | wc-l
```  
```
Ron
  grep -o -i Ron "Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt" | wc-l
```
```  
Dumbledore
   grep -o -i Dumbledore "Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt" | wc-l
```
```   
Hermoine
   grep -o -i Hermoine "Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt" | wc-l
```
5. Print lines from 100 through 200 in the book
```  
cat -n harry.txt | grep -A100 '\b100\b'
```
6.How many unique words are present in the book?
``` 
awk '{for(i = 1; i <= NF; i++) {a[$i]++}} END {for(k in a) if(a[k] == 1){counter++} print counter}' harry.txt
```
   Processes:
1.List your browser's process ids (pid) and parent process ids(ppid)
```   
 pidof chrome
 ps -o ppid = $(pidof chrome)
```
2.Stop the browser application from the command line
```   
kill $(pidof chrome)
```
3.List the top 3 processes by CPU usage.
     ```
        ps aux | sort -nrk 3,3 | head -n 3   
``` 
Managing Software:
1.Install `git`, `vim` and `nginx`
```
sudo apt-get install git

sudo apt-get install vim

sudo apt-get install nginx


```
2.Uninstall `nginx`
```   
sudo apt-get remove nginx nginx-common
```
Misc:
1.What's your local ip address?
```  
hostname -I
```
2.Find the ip address of `google.com`
```  
host www.google.com
```
3.Where is the `python` command located? What about `python3`?
   ```
   whereis python
   whereis python3
```


